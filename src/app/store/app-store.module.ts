import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import * as loginStore from '../login/store/states';
import * as loginReducers from '../login/store/reducers';
import { environment } from '../../environments/environment';
import { reducers, metaReducers } from './reducers';
import { ConfigStoreModule } from './config-claims/store';

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([]),
    StoreModule.forFeature(loginStore.featureName, loginReducers.reducer),
    ConfigStoreModule
  ],
})
export class AppStoreModule {}
