import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as configActions from '../actions';
import * as configSelectors from '../selectors';


@Injectable({
  providedIn: 'root',
})
export class ConfigFacade {
  vettingValidationCategoryEnabled$ = this.store$.pipe(
    select(configSelectors.vettingValidationCategoryEnabled)
  );
  timeoutDurationMinutes$ = this.store$.pipe(
    select(configSelectors.timeoutDurationMinutes)
  );
  patientInvitationLettersEnabled$ = this.store$.pipe(
    select(configSelectors.patientInvitationLettersEnabled)
  );
  pasDemographicsSourceSuppliesAllergies$ = this.store$.pipe(
    select(configSelectors.pasDemographicsSourceSuppliesAllergies)
  );
  panelListPageSize$ = this.store$.pipe(
    select(configSelectors.panelListPageSize)
  );
  enableScheduling$ = this.store$.pipe(
    select(configSelectors.enableScheduling)
  );

  constructor(private store$: Store) {}

  getConfig() {
    this.store$.dispatch(configActions.loadConfig());
  }

  getClaims() {
    this.store$.dispatch(configActions.loadClaims());
  }
}
