import { createReducer, on } from '@ngrx/store';
import  * as configActions from '../actions';
import { initialState, adapter } from '../states';

export const reducer = createReducer(
  initialState,
  /**
   * Config  load success.
   */
  on(configActions.loadConfigSuccess, (state, { config }) => ({
    ...state,
    config,
  })),

  on(configActions.loadConfigFailure, (state, { configError }) => ({
    ...state,
    configError,
  })),

  /**
   * claims  load success.
   */
  on(configActions.loadClaimsSuccess, (state, { authentication }) => ({
    ...state,
    authentication,
  })),
  on(configActions.loadClaimsFailure, (state, { authenticationError }) => ({
    ...state,
    authenticationError,
  })),
);

