import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State, featureName, adapter } from '../states';

/**
 * Selectors
 */

/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
 */
export const getConfigStateCreate = createFeatureSelector<State>(featureName);



/**
 * Every reducer module exports selector functions, however child reducers
 * have no knowledge of the overall state tree. To make them usable, we
 * need to make new selectors that wrap them.
 *
 * The createSelector function creates very efficient selectors that are memoized and
 * only recompute when arguments change. The created selectors can also be composed
 * together to select different pieces of state.
 */

 


export const getConfig = createSelector(getConfigStateCreate,(state: any) => state.config);

export const panelListPageSize = createSelector(getConfigStateCreate,(state: any) => {
  // not always safe to do this,
  // as it's not actually null coalescing,
  // however '??' support is currently in
  // draft stage: https://github.com/tc39/proposal-nullish-coalescing

  return state && state.config && state.config.panelListPageSize || 30;
});

export const pasDemographicsSourceSuppliesAllergies = createSelector(getConfigStateCreate,(state: any) => {
  return state && state.config && state.config.pasDemographicsSourceSuppliesAllergies !== undefined
    ? state.config.pasDemographicsSourceSuppliesAllergies
    : false;
});

export const enableScheduling = createSelector(getConfigStateCreate,(state: any) => {
  return state && state.config && state.config.enableScheduling !== undefined
    ? state.config.enableScheduling
    : false;
});

export const patientInvitationLettersEnabled = createSelector(getConfigStateCreate,(state: any) => {
  return state && state.config && state.config.patientInvitationLettersEnabled !== undefined
    ? state.config.patientInvitationLettersEnabled
    : false;
});

export const timeoutDurationMinutes = createSelector(getConfigStateCreate,(state: any) => {
  return state && state.config && state.config.timeoutDuration !== undefined
    ? state.config.timeoutDuration
    : 10;
});


// this is also working same below fuctions
export const vettingValidationCategoryEnabled = createSelector(getConfigStateCreate, (state) => {
  return state && state.config && state.config.vettingValidationCategoryEnabled !== undefined
    ? state.config.vettingValidationCategoryEnabled
    : true;
});

// this is also working 
// export const vettingValidationCategoryEnabled = (state: any) => {
//   return state && state.config && state.config.vettingValidationCategoryEnabled !== undefined
//     ? state.config.vettingValidationCategoryEnabled
//     : true;
// };

