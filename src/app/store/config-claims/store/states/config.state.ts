import { EntityState, createEntityAdapter } from '@ngrx/entity';


/**
 * Feature name
 */
export const featureName = 'access';

/**
 * State
 */
export interface State extends EntityState<any> {
  config: any
  authentication: any
}

/**
 * Adapter
 */
export const adapter = createEntityAdapter<any>();

/**
 * Initial state
 */
export const initialState: State = adapter.getInitialState({
  config: null,
  authentication: null
});
