import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ConfigService } from 'src/app/@core/service/config.service';
import * as configActions  from '../actions';

@Injectable()
export class ConfigEffects {
  loadConfig$ = createEffect(() =>
    this.actions$.pipe(
      ofType(configActions.loadConfig),
      mergeMap(() =>
        this.configService.getConfig().pipe(
          map((config) => configActions.loadConfigSuccess({ config })),
          catchError(({ message }: HttpErrorResponse) =>
            of(configActions.loadConfigFailure({ configError: message }))
          )
        )
      )
    )
  );

   loadClaims$ = createEffect(() =>
    this.actions$.pipe(
      ofType(configActions.loadClaims),
      mergeMap(() =>
        this.configService.getClaims().pipe(
          map((authentication) => configActions.loadClaimsSuccess({ authentication })),
          catchError(({ message }: HttpErrorResponse) =>
            of(configActions.loadClaimsFailure({ authenticationError: message }))
          )
        )
      )
    )
   );

 
  constructor(
    private actions$: Actions,
    private configService: ConfigService
  ) {}
}
