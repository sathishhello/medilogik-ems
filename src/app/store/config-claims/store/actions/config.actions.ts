import { createAction, props } from '@ngrx/store';

// config
export const loadConfig = createAction('[config] Load');

export const loadConfigSuccess = createAction(
  '[config] Load Success',
  props<{ config: any }>()
);

export const loadConfigFailure = createAction(
  '[config] Load Failure',
  props<{ configError: string }>()
);

// Claims

export const loadClaims = createAction('[claims] Load');

export const loadClaimsSuccess = createAction(
  '[claims] Load Success',
  props<{ authentication: any }>()
);

export const loadClaimsFailure = createAction(
  '[claims] Load Failure',
  props<{ authenticationError: string }>()
);
