import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AwaitingVettingComponent } from './awaiting-vetting.component';

describe('AwaitingVettingComponent', () => {
  let component: AwaitingVettingComponent;
  let fixture: ComponentFixture<AwaitingVettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AwaitingVettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwaitingVettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
