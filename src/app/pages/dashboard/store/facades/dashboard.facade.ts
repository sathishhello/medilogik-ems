import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as dashboardActions from '../actions';
import * as dashboardSelectors from '../selectors';


@Injectable({
  providedIn: 'root',
})
export class DashboardFacade {
 
  activeEpisodes$ = this.store$.pipe(
    select(dashboardSelectors.getDashboardActiveEpisodes)
  );

  constructor(private store$: Store) {
    console.log('checking panel data')
    this.activeEpisodes$.subscribe(value => console.log(value))
  }

  getActiveEpisodes() {
    
     this.store$.dispatch(dashboardActions.loadActiveEpisodes());
  }

  
}
