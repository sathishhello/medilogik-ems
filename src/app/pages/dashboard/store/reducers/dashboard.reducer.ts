import { createReducer, on } from '@ngrx/store';
import  * as dashboardActions from '../actions';
import { initialState, adapter } from '../states';

export const reducer = createReducer(
  initialState,
  /**
   * Config  load success.
   */
  on(dashboardActions.loadActiveEpisodesSuccess, (state,  data ) => ({
    ...state,
    panelData : data.data
  })),

  on(dashboardActions.loadActiveEpisodesFailure, (state, { activeEpisodesError }) => ({
    ...state,
    activeEpisodesError,
  })),


);

