import { createAction, props } from '@ngrx/store';
import { ActiveEpisodesDashboardDataDto } from '../../models';

// ActiveEpisodes
export const loadActiveEpisodes = createAction(
  '[Dashboard] Active Episodes Load'
);

export const loadActiveEpisodesSuccess = createAction(
  '[Dashboard] Active Episodes Load Success',
   props<{ data: any }>());

export const loadActiveEpisodesFailure = createAction(
  '[Dashboard] Active Episodes Load Load Failure',
  props<{ activeEpisodesError: string }>()
);

// AwaitingVetting
export const loadAwaitingVetting = createAction(
  '[Dashboard] Awaiting Vetting Load'
);

export const loadAwaitingVettingSuccess = createAction(
  '[Dashboard] Awaiting Vetting Load Success',
  props<{ config: any }>()
);

export const loadAwaitingVettingFailure = createAction(
  '[Dashboard] Awaiting Vetting Load Failure',
  props<{ configError: string }>()
);
