import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { ActiveEpisodesDashboardDataDto } from '../../models/dashboard.model';

/**
 * Feature name
 */
export const featureName = 'dashboardActiveEpisodes';

/**
 * State
 */

export interface DashboardActiveEpisodes {
  panelData?: ActiveEpisodesDashboardDataDto[] | null;
  totalRowCount?: number;
  pageNumber?: number;
  isLoading?: boolean;
  filters?: any[];
  sortOrder?: any[];
  name?: string;
  siteId?: number | null;
}

export interface State extends EntityState<DashboardActiveEpisodes> {
  panelData?: ActiveEpisodesDashboardDataDto[] | null;
  totalRowCount?: number;
  pageNumber?: number;
  isLoading?: boolean;
  filters?: any[];
  sortOrder?: any[];
  name?: string;
  siteId?: number | null;
}

/**
 * Adapter
 */
export const adapter = createEntityAdapter<DashboardActiveEpisodes>();

/**
 * Initial state
 */

export const initialState: State = adapter.getInitialState({
  panelData: null,
  totalRowCount: 0,
  pageNumber: 1,
  isLoading: false,
  filters: [],
  sortOrder: [],
  name: 'Active Episodes',
  siteId: null,
});
