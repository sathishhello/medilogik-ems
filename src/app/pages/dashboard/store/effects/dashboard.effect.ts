import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { DashboardService } from '../../services/dashboard.service';
import * as dashboardActions from '../actions';
import { ActiveEpisodesDashboardDataDto } from '../../models';

@Injectable()
export class DashboardEffects {
  loadActiveEpisodes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dashboardActions.loadActiveEpisodes),
      mergeMap(() =>
        this.dashboardService.getActiveEpisodes().pipe(
          map((data: any) => 
            dashboardActions.loadActiveEpisodesSuccess({data})
          ),
          catchError(({ message }: HttpErrorResponse) =>
            of(
              dashboardActions.loadActiveEpisodesFailure({
                activeEpisodesError: message,
              })
            )
          )
        )
      )
    )
  );

  loadAwaitingVetting$ = createEffect(() =>
    this.actions$.pipe(
      ofType(dashboardActions.loadAwaitingVetting),
      mergeMap(() =>
        this.dashboardService.getAwaitingVetting().pipe(
          map((config) =>
            dashboardActions.loadAwaitingVettingSuccess({ config })
          ),
          catchError(({ message }: HttpErrorResponse) =>
            of(
              dashboardActions.loadAwaitingVettingFailure({
                configError: message,
              })
            )
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private dashboardService: DashboardService
  ) {}
}
