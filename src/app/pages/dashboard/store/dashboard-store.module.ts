import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { featureName } from './states';
import { reducer } from './reducers';
import { DashboardEffects } from './effects';


@NgModule({
  imports: [
    StoreModule.forFeature(featureName, reducer),
    EffectsModule.forFeature([DashboardEffects]),
  ],
})
export class DashboardStoreModule {}
