import { createFeatureSelector, createSelector } from '@ngrx/store';
import { featureName, State } from '../states';


export const getDashboardStateCreate = createFeatureSelector<State>(
  featureName
);

export const getDashboardActiveEpisodes = createSelector(
  getDashboardStateCreate,
  (state: any) => state
);
