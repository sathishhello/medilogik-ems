import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActiveEpisodesComponent } from './active-episodes/active-episodes.component';
import { AuditLogComponent } from './audit-log/audit-log.component';
import { AwaitingVettingComponent } from './awaiting-vetting/awaiting-vetting.component';
import { DailyListComponent } from './daily-list/daily-list.component';
import { DashboardComponent } from './dashboard.component';
import { EpisodeLocksComponent } from './episode-locks/episode-locks.component';
import { ReportingComponent } from './reporting/reporting.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {
    path:'',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: 'active-episodes'
      },
      {
        path: 'active-episodes',
        component: ActiveEpisodesComponent
      },
      {
        path: 'daily-list',
        component: DailyListComponent
      },
      {
        path: 'audit-log',
        component: AuditLogComponent
      },
      {
        path: 'episode-locks',
        component: EpisodeLocksComponent
      },
      {
        path: 'reporting',
        component: ReportingComponent
      },
      {
        path: 'summary',
        component: SummaryComponent
      },
      {
        path: 'awaiting-vetting',
        component: AwaitingVettingComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
