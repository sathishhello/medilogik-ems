export interface ActiveEpisodesDashboardDataDto {
  canCancelScheduledEpisode?: boolean;
  patientLocalIdentifier?: string | undefined;
  patientFullName?: string | undefined;
  patientCategory?: string | undefined;
  episodeReferralDate?: string | undefined;
  roomName?: string | undefined;
  procedureCombinationName?: string | undefined;
  episodeProcedureDate?: string | undefined;
  endoscopistFullName?: string | undefined;
  patientStatus?: string | undefined;
  episodeId?: number;
}
