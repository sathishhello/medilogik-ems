import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ActiveEpisodesComponent } from './active-episodes/active-episodes.component';
import { DashboardStoreModule } from './store/dashboard-store.module';
import { DailyListComponent } from './daily-list/daily-list.component';
import { AuditLogComponent } from './audit-log/audit-log.component';
import { EpisodeLocksComponent } from './episode-locks/episode-locks.component';
import { SummaryComponent } from './summary/summary.component';
import { ReportingComponent } from './reporting/reporting.component';
import { AwaitingVettingComponent } from './awaiting-vetting/awaiting-vetting.component';


@NgModule({
  declarations: [DashboardComponent, ActiveEpisodesComponent, DailyListComponent, AuditLogComponent, EpisodeLocksComponent, SummaryComponent, ReportingComponent, AwaitingVettingComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    DashboardStoreModule
  ]
})
export class DashboardModule { }
