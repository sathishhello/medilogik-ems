import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-daily-list',
  templateUrl: './daily-list.component.html',
  styleUrls: ['./daily-list.component.css']
})
export class DailyListComponent implements OnInit {
  dailyList: any;

  constructor(private _http: HttpClient) { }

  ngOnInit(): void {
    this._http.get('http://localhost:3000/dailylist').subscribe((r)=> {
      console.log(r)
      this.dailyList = r;
    }); 
  }

}
