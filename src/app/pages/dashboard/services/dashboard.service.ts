import { Injectable } from '@angular/core';
import { CommonHttpService } from 'src/app/@core/service/common-http.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private _http: HttpClient, private _httpC: CommonHttpService) {}


  getActiveEpisodes() {
    //  return this._http.get('/api/dashboard/active-episodes?');
     return this._http.get('http://localhost:3000/activeepisode');
     
  }

  getAwaitingVetting() {
    return this._http.get('/api/dashboard/awaiting-vetting?');
 }

  
}
