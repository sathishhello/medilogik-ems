import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DashboardFacade } from '../store/facades';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-active-episodes',
  templateUrl: './active-episodes.component.html',
  styleUrls: ['./active-episodes.component.css']
})
export class ActiveEpisodesComponent implements OnInit {
  activeEpisodes$: Observable<any>;

  constructor(private dashboardFacade : DashboardFacade) {
    this.activeEpisodes$ =  this.dashboardFacade.activeEpisodes$ 
    // console.log('compound testing')
    // this.dashboardFacade.activeEpisodes$.subscribe(value => console.log(value))
   }

  ngOnInit(): void {
    this.dashboardFacade.getActiveEpisodes();
  }

 
}