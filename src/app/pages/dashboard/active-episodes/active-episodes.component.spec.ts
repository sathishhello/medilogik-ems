import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveEpisodesComponent } from './active-episodes.component';

describe('ActiveEpisodesComponent', () => {
  let component: ActiveEpisodesComponent;
  let fixture: ComponentFixture<ActiveEpisodesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveEpisodesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveEpisodesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
