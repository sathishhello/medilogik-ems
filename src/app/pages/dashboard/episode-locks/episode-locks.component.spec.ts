import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpisodeLocksComponent } from './episode-locks.component';

describe('EpisodeLocksComponent', () => {
  let component: EpisodeLocksComponent;
  let fixture: ComponentFixture<EpisodeLocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpisodeLocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpisodeLocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
