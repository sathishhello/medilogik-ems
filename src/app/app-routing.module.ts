import { APP_BASE_HREF } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import {PagesModule} from './pages/pages.module'
import { NotFoundComponent } from './pages/not-found/not-found.component';
// import { LoginComponent } from './shared/component/login/login.component';
const PROVIDERS = [
    {
        provide: APP_BASE_HREF,
        useValue: "/"
    }
];

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login',
  },
  {
    path: 'ems',
    loadChildren: () =>
      import('./pages/pages.module').then((m) => m.PagesModule),
  },
  // {
  //   path: 'login',
  //   component: LoginComponent
  // },
  {
    path: 'not-found',
    component: NotFoundComponent,
  },
  { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'not-found',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ useHash: false})],
  exports: [RouterModule],
  providers: PROVIDERS
})
export class AppRoutingModule { }
