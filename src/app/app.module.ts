import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import { AppStoreModule } from './store';
import { CoreModule } from './@core/core.module';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    AppStoreModule,
    AppRoutingModule,
    CoreModule
  ],
  providers: [{provide: LocationStrategy, useClass: PathLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
