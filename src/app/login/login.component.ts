import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginFacade } from './store/facades';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  login$: any;

  constructor(private loginFacade: LoginFacade, private _router: Router) {}

  ngOnInit(): void {
    this.login$ = this.loginFacade.login$;

    this.login$.subscribe((login: any) => {
      if (login.isLogin) {
        console.log(login);
        this._router.navigate(['/ems']);
      }
    });
  }

  login() {
    this.loginFacade.getLogin({
      email: 'admin@cardinality.ai',
      password: 'test@123',
      isLogin: true,
    });
  }
}
