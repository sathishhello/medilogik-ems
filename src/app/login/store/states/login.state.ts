import { EntityState, createEntityAdapter } from '@ngrx/entity';


/**
 * Feature name
 */
export const featureName = 'login';



/**
 * State
 */
export interface State extends EntityState<any> {
  email?: string,
  password?: string,
  isLogin?: boolean
}

/**
 * Adapter
 */
export const adapter = createEntityAdapter<any>();

/**
 * Initial state
 */
export const initialState: State = adapter.getInitialState({
  isLogin: false
});
