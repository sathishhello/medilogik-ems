import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as loginActions from '../actions';
import * as loginSelectors from '../selectors';

@Injectable({
  providedIn: 'root',
})
export class LoginFacade {
  login$ = this.store$.pipe(select(loginSelectors.getLoginDetails));

  constructor(private store$: Store) {}

  getLogin(inputReq: any) {
    this.store$.dispatch(loginActions.loadLoginSuccess(inputReq));
  }
}
