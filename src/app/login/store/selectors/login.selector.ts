import { createFeatureSelector, createSelector } from '@ngrx/store';

import { State, featureName, adapter } from '../states';

/**
 * Selectors
 */

/**
 * The createFeatureSelector function selects a piece of state from the root of the state object.
 * This is used for selecting feature states that are loaded eagerly or lazily.
 */
export const getLoginState = createFeatureSelector<State>(featureName);

export const getLoginDetails = createSelector(getLoginState, (state) => state);

