import { createReducer, on } from '@ngrx/store';
import * as loginActions from '../actions';
import { initialState, adapter } from '../states';

export const reducer = createReducer(
  initialState,
  /**
   * login  load success.
   */
  on(loginActions.loadLoginSuccess, (state, { email ,password , isLogin }) => ({
    ...state,
    email,password,isLogin
  })),

  on(loginActions.loadLoginFailure, (state, { loginError }) => ({
    ...state,
    loginError,
  }))
);
