import { createAction, props } from '@ngrx/store';

// config
export const loadLogin = createAction('[Login] Load');

export const loadLoginSuccess = createAction(
  '[login] Load Success',
  props<{ 
    email : string,
    password: string,
    isLogin : boolean
   }>()
);

export const loadLoginFailure = createAction(
  '[login] Load Failure',
  props<{ loginError: string }>()
);

