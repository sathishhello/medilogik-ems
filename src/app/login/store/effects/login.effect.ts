import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ConfigService } from 'src/app/@core/service/config.service';
import * as loginActions from '../actions'
import { concatMap } from 'rxjs/operators';
@Injectable()
export class LoginEffects {


  constructor(
    private actions$: Actions,
    private configService: ConfigService
  ) {}
}
