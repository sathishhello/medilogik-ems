import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigFacade } from './store/config-claims/store/facades';
import { LoginFacade } from './login/store/facades';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'a10';
  showVettingValidationCategory$: Observable<any>;
  configData$: void;
  claimsData$: void;
  login$: Observable<any>;

  constructor(
    private configFacade: ConfigFacade,
    private loginFacade: LoginFacade,
    private _router: Router
  ) {
    // call client config API
    this.configData$ = this.configFacade.getConfig();
    // call Claims API
    this.claimsData$ = this.configFacade.getClaims();

    this.login$ = this.loginFacade.login$;

    this.login$.subscribe((login: any) => {
      if (login.isLogin) {
        // alert('login  Success');
        this._router.navigate(['/ems']);
      } else {
        // alert('logout Success');
        this._router.navigate(['/login']);
      }
    });

    // get value vettingValidationCategoryEnabled from client config API
    this.showVettingValidationCategory$ = this.configFacade.vettingValidationCategoryEnabled$;
  }
}
