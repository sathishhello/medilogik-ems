import { CommonModule } from '@angular/common';
import {CommonHttpService} from './service/common-http.service'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthTokenInterceptor } from '../@core/interceptor/auth-token.interceptor';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [FooterComponent],
  exports: [FooterComponent],
  imports: [CommonModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthTokenInterceptor, multi: true },
     CommonHttpService
  ],
})
export class CoreModule {}
