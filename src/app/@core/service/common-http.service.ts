import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {environment } from '../../../environments/environment';

@Injectable()
export class CommonHttpService {
  baseUrl: string = environment.apiHost;

  constructor(private _http: HttpClient) {}

  public get(req: string): Observable<any> {
    return this._http.get(this.baseUrl + req);
  }

  public create(url: any, body:any, options?: any): Observable<any> {
    return this._http.post(this.baseUrl + url, body, options);
  }
  
}
