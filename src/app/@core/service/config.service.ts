import { Injectable } from '@angular/core';
import { CommonHttpService } from './common-http.service';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  constructor(private _http: CommonHttpService) {}

  getConfig() {
    return this._http.get('/api/clientconfiguration');
  }

  getClaims() {
    return this._http.get('/api/account/claims');
  }
}
