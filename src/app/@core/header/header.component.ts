import { Component, OnInit } from '@angular/core';
import { props, Store } from '@ngrx/store';
import { LoginFacade } from 'src/app/login/store/facades';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  login$: any;
  isLogin: boolean = false;

  constructor(private loginFacade: LoginFacade) {}

  ngOnInit(): void {
    this.login$ = this.loginFacade.login$;

    this.login$.subscribe((login: any) => {
      if (login && login.isLogin) {
        console.log('Header');
        console.log(login);
        this.isLogin = true;
      }
    });
  }
}
