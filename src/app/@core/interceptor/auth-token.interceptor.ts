import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authToken = 'f5ecfb306e60620010bb76308e041dc8ca96ba5fca7967d574bfef606d2482e1';
    if(authToken) {
      request = request.clone({
        headers: request.headers.set('TestAuthToken',authToken)
      })
    }
    return next.handle(request);
  }
}
